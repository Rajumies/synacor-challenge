import numpy as np
import sys
from numpy import uint16

MODULO = 32768

HALT = 0
SET = 1
PUSH = 2
POP = 3
EQ = 4
GT = 5
JMP = 6
JT = 7
JF = 8
ADD = 9
MULT = 10
MOD = 11
AND = 12
OR = 13
NOT = 14
RMEM = 15
WMEM = 16
CALL = 17
RET = 18
OUT = 19
IN = 20
NOOP = 21

one_arg = (PUSH, POP, JMP, CALL, OUT, IN)
two_arg = (SET, JT, JF, NOT, RMEM, WMEM)
three_arg = (EQ, GT, ADD, MULT, MOD, AND, OR)



class VirtualMachine:

    REGISTER_0 = 32768
    REGISTER_7 = 32775
    BITNESS = 2**15

    def __init__(self):
        self.memory = [0] * (self.BITNESS + 8)
        self.registers = self.memory[self.BITNESS:]
        self.stack = []
        self.ip = 0
        self.running = False
        self.char_buffer = []

    def load(self, data):
        for index, byte in enumerate(data):
            self.memory[index] = int(byte)

    def run(self):
        self.running = True
        while self.running:
            instr = self.memory[self.ip]
            self.handle_instruction(instr)

    def handle_instruction(self, opcode):
        args = self.get_args(opcode)

        if opcode == HALT:
            self.running = False

        elif opcode == SET:
            self.op_set(*args)

        elif opcode == PUSH:
            self.op_push(*args)

        elif opcode == POP:
            self.op_pop(*args)

        elif opcode == EQ:
            self.op_eq(*args)

        elif opcode == GT:
            self.op_gt(*args)

        elif opcode == JMP:
            self.op_jmp(*args)
            return

        elif opcode == JT:
            if self.op_jt(*args):
                return

        elif opcode == JF:
            if self.op_jf(*args):
                return

        elif opcode == ADD:
            self.op_add(*args)

        elif opcode == MULT:
            self.op_mult(*args)

        elif opcode == MOD:
            self.op_mod(*args)

        elif opcode == AND:
            self.op_and(*args)

        elif opcode == OR:
            self.op_or(*args)

        elif opcode == NOT:
            self.op_not(*args)

        elif opcode == RMEM:
            self.op_rmem(*args)

        elif opcode == WMEM:
            self.op_wmem(*args)

        elif opcode == CALL:
            self.op_call(*args)
            return

        elif opcode == RET:
            self.op_ret()
            return

        elif opcode == OUT:
            self.op_out(*args)

        elif opcode == IN:
            self.op_in(*args)

        elif opcode == NOOP:
            pass
        else:
            sys.exit("Error: Invalid opcode: " + str(opcode))

        self.ip += len(args) + 1

    def op_set(self, address, value):
        self.write_register(address, value)

    def op_push(self, addr):
        value = self.read_value(addr)
        self.stack += [value]

    def op_pop(self, address):
        value = self.stack.pop()
        self.write_register(address, value)

    def op_eq(self, address, value1, value2):
        self.write_register(address, int(value1 == value2))

    def op_gt(self, address, value1, value2):
        self.write_register(address, int(value1 > value2))

    def op_jmp(self, addr):
        value = self.read_value(addr)
        self.ip = value

    def op_jt(self, addr, target):
        value = self.read_value(addr)
        if value:
            self.ip = target
        return value

    def op_jf(self, addr, target):
        value = self.read_value(addr)
        if not value:
            self.ip = target
        return not value

    def op_add(self, address, value1, value2):
        result = (value1 + value2) % MODULO
        self.write_register(address, result)

    def op_mult(self, address, value1, value2):
        result = (value1 * value2) % MODULO
        self.write_register(address, result)

    def op_mod(self, address, dividend, divisor):
        result = dividend % divisor
        self.write_register(address, result)

    def op_and(self, address, value1, value2):
        result = value1 & value2
        self.write_register(address, result)

    def op_or(self, address, value1, value2):
        result = value1 | value2
        self.write_register(address, result)

    def op_not(self, address, value):
        result = ~value % self.BITNESS
        self.write_register(address, result)

    def op_rmem(self, reg_address, mem_address):
        value = self.memory[mem_address]
        self.write_register(reg_address, value)

    def op_wmem(self, mem_address, value):
        mem_address = self.read_value(mem_address)
        self.memory[mem_address] = value

    def op_call(self, target):
        self.stack += [self.ip + 2]
        self.ip = self.read_value(target)

    def op_ret(self):
        self.ip = self.stack.pop()

    def op_out(self, addr):
        value = self.read_value(addr)
        print(chr(value), end='')

    def op_in(self, address):
        if not self.char_buffer:
            self.char_buffer = list(input('> ') + '\n')

        char = self.char_buffer.pop(0)
        self.write_register(address, ord(char))


    def get_args(self, instr):
        args = []
        if instr in one_arg + two_arg + three_arg:
            args += [self.ip + 1]
        if instr in two_arg + three_arg:
            args += [self.read_value(self.ip + 2)]
        if instr in three_arg:
            args += [self.read_value(self.ip + 3)]
        return tuple(args)

    def read_value(self, arg):
        value = self.memory[arg]
        if value >= self.BITNESS:
            value = self.memory[value]
        return value

    def write_register(self, address, value):
        self.memory[self.memory[address]] = value
        self.registers = self.memory[self.BITNESS:]


with open("challenge.bin") as file:
    binary_data = np.fromfile(file, dtype=uint16)

vm = VirtualMachine()
vm.load(binary_data)
vm.run()
