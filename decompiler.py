import numpy as np
from numpy import uint16

class Decompiler:
    # A fairly crude decompiler just to get a glimpse of the program flow and its output

    # opcode string variants and # of arguments
    instructions = {
        0: ("HALT", 0),
        1: ("SET", 2),
        2: ("PUSH", 1),
        3: ("POP", 1),
        4: ("EQ", 3),
        5: ("GT", 3),
        6: ("JMP", 1),
        7: ("JT", 2),
        8: ("JF", 2),
        9: ("ADD", 3),
        10: ("MULT", 3),
        11: ("MOD", 3),
        12: ("AND", 3),
        13: ("OR", 3),
        14: ("NOT", 2),
        15: ("RMEM", 2),
        16: ("WMEM", 2),
        17: ("CALL", 1),
        18: ("RET", 0),
        19: ("OUT", 1),
        20: ("IN", 1),
        21: ("NOOP", 0)
    }
    BITNESS = 2**15

    def __init__(self):
        self.ip = 0
        self.memory = [0] * self.BITNESS
        self.decompiled_raw = []
        self.decompiled_nice = []

    def load(self, data):
        for index, byte in enumerate(data):
            self.memory[index] = int(byte)

    def decompile(self):
        while self.ip < len(self.memory):
            self.decode_instruction()
        self.format_decompiled()

    def decode_instruction(self):
        opcode = self.memory[self.ip]
        if opcode not in self.instructions:
            self.decompiled_raw.append([str(self.ip), str(opcode), ""])
            self.ip += 1
            return
        instr = self.instructions[opcode][0]
        args = []
        for arg in range(1, self.instructions[opcode][1] + 1):
            value = self.memory[self.ip + arg]
            if value >= self.BITNESS:
                value = "r" + str(value % 32768)
            elif instr == "OUT":
                value = chr(value)
            args += [value]
        self.decompiled_raw.append([str(self.ip), instr, args])
        self.ip += self.instructions[opcode][1] + 1

    def format_decompiled(self):
        for op in self.decompiled_raw:
            if self.decompiled_nice[-1:]:
                if self.decompiled_nice[-1][1] == "OUT":
                    if op[1] == "OUT":
                        self.decompiled_nice[-1][2] += op[2]
                        continue
                    else:
                        self.decompiled_nice[-1][2] = ["".join(self.decompiled_nice[-1][2])[:-1]]

            self.decompiled_nice.append(op)

    def dump(self):
        print("| Memory address | Instruction | Arguments |")
        print("--------------------------------------------")
        for op in self.decompiled_nice:
            mem_addr = str(op[0])
            opcode = op[1]
            args = op[2]
            print('{:5s}: '.format(mem_addr), end='')
            print('{:4s} '.format(opcode), end='')
            for arg in args:
                print(', {:5s} '.format(str(arg)), end='')
            print("")

with open("challenge.bin") as file:
    binary_data = np.fromfile(file, dtype=uint16)

decompiler = Decompiler()
decompiler.load(binary_data)
decompiler.decompile()
decompiler.dump()


